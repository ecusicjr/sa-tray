﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace SATray
{
    public partial class Form1 : Form
    {
        public Client c;
        public Form1()
        {
            InitializeComponent();
            string input = Interaction.InputBox("What is your SupervisorAssist username?", "SA Tray", "agent1");
            label2.Text = input;
            c = new Client("https://sa1.fairmountcharles.com:8443/", label2.Text);
            label2.Text = c.AgentU;
            timer1.Interval = 1000 * c.token.ExpiresIn;
            timer1.Enabled = true;
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) { 
            this.Show();
            this.WindowState = FormWindowState.Normal;
            //Determine "rightmost" screen
            Screen rightmost = Screen.AllScreens[0];
            foreach (Screen screen in Screen.AllScreens)
            {
                if (screen.WorkingArea.Right > rightmost.WorkingArea.Right)
                    rightmost = screen;
            }

            this.Left = rightmost.WorkingArea.Right - this.Width;
            this.Top = rightmost.WorkingArea.Bottom - this.Height;
            notifyIcon1.Visible = false;
            }
            else
            {
                contextMenuStrip1.Show();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            
        }
        bool switcher = false;
        string HighReason = "5 calls in 1 week. High risk";
        private void button3_Click(object sender, EventArgs e)
        {
            switcher = !switcher;
            if (switcher)
            {
                button1.Enabled = false;
                button2.Enabled = false;
                toolStripMenuItem1.Enabled = false;
                toolStripMenuItem2.Enabled = false;
                string input = Interaction.InputBox("What is your reason for this escalation?", "SA Tray", HighReason);
                HighReason = input;
                c.Escalate("HIGH", HighReason);
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
                toolStripMenuItem1.Enabled = true;
                toolStripMenuItem2.Enabled = true;
                c.Deescalate("HIGH", HighReason);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string input = Interaction.InputBox("What is your SupervisorAssist username?", "SA Tray", label2.Text);
            label2.Text = input;
            while (!c.FindAgent(label2.Text))
            {
                input = Interaction.InputBox("Agent not found. What is your SupervisorAssist username?", "SA Tray", label2.Text);
                label2.Text = input;
            }
        }
        string MedReason = "This is an unfamiliar topic";
        private void button2_Click(object sender, EventArgs e)
        {
            switcher = !switcher;
            if (switcher)
            {
                button1.Enabled = false;
                button3.Enabled = false;
                toolStripMenuItem1.Enabled = false;
                toolStripMenuItem3.Enabled = false;
                string input = Interaction.InputBox("What is your reason for this escalation?", "SA Tray", MedReason);
                MedReason = input;
                c.Escalate("MEDIUM", MedReason);
            }
            else
            {
                button1.Enabled = true;
                button3.Enabled = true;
                toolStripMenuItem1.Enabled = true;
                toolStripMenuItem3.Enabled = true;
                c.Deescalate("MEDIUM", MedReason);
            }
        }
        string LowReason = "I have a question";
        private void button1_Click(object sender, EventArgs e)
        {
            switcher = !switcher;
            if (switcher)
            {
                button2.Enabled = false;
                button3.Enabled = false;
                toolStripMenuItem2.Enabled = false;
                toolStripMenuItem3.Enabled = false;
                string input = Interaction.InputBox("What is your reason for this escalation?", "SA Tray", LowReason);
                LowReason = input;
                c.Escalate("INFO", LowReason);
            }
            else
            {
                button2.Enabled = true;
                button3.Enabled = true;
                toolStripMenuItem2.Enabled = true;
                toolStripMenuItem3.Enabled = true;
                c.Deescalate("INFO", LowReason);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            switcher = !switcher;
            if (switcher)
            {
                button2.Enabled = false;
                button3.Enabled = false;
                toolStripMenuItem2.Enabled = false;
                toolStripMenuItem3.Enabled = false;
                string input = Interaction.InputBox("What is your reason for this escalation?", "SA Tray", LowReason);
                LowReason = input;
                c.Escalate("INFO", LowReason);
            }
            else
            {
                button2.Enabled = true;
                button3.Enabled = true;
                toolStripMenuItem2.Enabled = true;
                toolStripMenuItem3.Enabled = true;
                c.Deescalate("INFO", LowReason);
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            switcher = !switcher;
            if (switcher)
            {
                button1.Enabled = false;
                button3.Enabled = false;
                toolStripMenuItem1.Enabled = false;
                toolStripMenuItem3.Enabled = false;
                string input = Interaction.InputBox("What is your reason for this escalation?", "SA Tray", MedReason);
                MedReason = input;
                c.Escalate("MEDIUM", MedReason);
            }
            else
            {
                button1.Enabled = true;
                button3.Enabled = true;
                toolStripMenuItem1.Enabled = true;
                toolStripMenuItem3.Enabled = true;
                c.Deescalate("MEDIUM", MedReason);
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            switcher = !switcher;
            if (switcher)
            {
                button1.Enabled = false;
                button2.Enabled = false;
                toolStripMenuItem1.Enabled = false;
                toolStripMenuItem2.Enabled = false;
                string input = Interaction.InputBox("What is your reason for this escalation?", "SA Tray", HighReason);
                HighReason = input;
                c.Escalate("HIGH", HighReason);
            }
            else
            {
                button1.Enabled = true;
                button2.Enabled = true;
                toolStripMenuItem1.Enabled = true;
                toolStripMenuItem2.Enabled = true;
                c.Deescalate("HIGH", HighReason);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            c.RefreshToken();
            timer1.Interval = c.token.ExpiresIn * 1000;
            timer1.Enabled = true;
        }
    }
}
