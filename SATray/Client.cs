﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System.Net;
using System.Timers;

namespace SATray
{
    public class Client
    {
        RestClient client;
        public Token token;
        string url;
        List<MonitorSession> MonitorSessions;
        string AgentId;
        public string AgentU;
        public Client(string url, string AgentId)
        {
            this.url = url;
            ServicePointManager.ServerCertificateValidationCallback +=
        (sender, certificate, chain, sslPolicyErrors) => true;
            client = new RestClient(url);
            client.Authenticator = new SimpleAuthenticator("username","fcService","password", "H@ckerPr00f");
            RestRequest request = new RestRequest("authserver/token", Method.POST);
            
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("client_id", "apprentice");
            request.AddParameter("username", "fcService");
            request.AddParameter("password", "H@ckerPr00f");
            
            request.AddParameter("grant_type", "password");
            request.AddParameter("provider", "sadb-based-auth-default");
            
            var response = client.Execute(request);
            //System.Windows.Forms.MessageBox.Show(response.Content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                JsonDeserializer deserial = new JsonDeserializer();
                token = deserial.Deserialize<Token>(response);
                GetMembers();
                AgentU = AgentId;
                while (!FindAgent(AgentId))
                { 
                    string input = Microsoft.VisualBasic.Interaction.InputBox("Agent not found. What is your SupervisorAssist username?", "SA Tray", AgentId);
                    AgentId = input;
                    AgentU = input;
                }
            }
        }
        public bool GetMembers()
        {
            RestRequest request = new RestRequest("sa/api/external/globaldashboard", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token.AccessToken);
            var response = client.Execute(request);
            //System.Windows.Forms.MessageBox.Show(response.Content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                JsonDeserializer deserial = new JsonDeserializer();
                MonitorSessions = deserial.Deserialize<List<MonitorSession>>(response);
                //System.Windows.Forms.MessageBox.Show(response.Content);
                return true;
            }
            else
            {
                return false;
            }
        }
        public void RefreshToken()
        {
            client = new RestClient(url);
            RestRequest request = new RestRequest("authserver/token", Method.POST);

            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("refresh_token", token.RefreshToken);
            request.AddParameter("client_id", "apprentice");
            request.AddParameter("grant_type", "refresh_token");

            var response = client.Execute(request);
            //System.Windows.Forms.MessageBox.Show(response.Content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                JsonDeserializer deserial = new JsonDeserializer();
                token = deserial.Deserialize<Token>(response);
                GetMembers();
            }
        }
        public bool FindAgent(string AgentId)
        {
            AgentU = AgentId;
            foreach (MonitorSession session in MonitorSessions)
            {
                foreach (Session s in session.Sessions)
                {
                    foreach (Member m in s.Members)
                    {
                        if (m.User.Username.Equals(AgentId))
                        {
                            this.AgentId = m.User.Id;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public bool Escalate(string priority, string reason)
        {
            client = new RestClient(url);
            RestRequest request = new RestRequest("sa/api/external/escalation/agent/"+AgentId, Method.POST);
            request.AddHeader("Authorization", "Bearer " + token.AccessToken);
            request.AddParameter("application/json", "{\"type\": \"" + priority + "\", \"context\":\"" + reason + "\"}", ParameterType.RequestBody);
            var response = client.Execute(request);
            //System.Windows.Forms.MessageBox.Show(response.Content);
            if (response.StatusCode == HttpStatusCode.NoContent)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Deescalate(string priority, string reason)
        {
            client = new RestClient(url);
            RestRequest request = new RestRequest("sa/api/external/escalation/agent/"+AgentId+"/deescalate", Method.POST);
            request.AddHeader("Authorization", "Bearer " + token.AccessToken);
            request.AddParameter("application/json", "{\"type\": \""+priority+"\", \"context\":\""+reason+"\"}", ParameterType.RequestBody);
            var response = client.Execute(request);
            //System.Windows.Forms.MessageBox.Show(response.Content);
            if (response.StatusCode == HttpStatusCode.NoContent)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class Token
    {
        [DeserializeAs(Name = "access_token")]
        public string AccessToken { get; set; }
        [DeserializeAs(Name = "refresh_token")]
        public string RefreshToken { get; set; }
        [DeserializeAs(Name = "expires_in")]
        public int ExpiresIn { get; set; }
    }
    public class MonitorSession
    {
        [DeserializeAs(Name = "monitorSessions")]
        public List<Session> Sessions { get; set; }
    }
    public class Session
    {
        [DeserializeAs(Name = "members")]
        public List<Member> Members { get; set; }
    }
    public class Member
    {
        [DeserializeAs(Name = "user")]
        public User User { get; set; }
    }
    public class User
    {
        [DeserializeAs(Name = "id")]
        public string Id { get; set; }
        [DeserializeAs(Name = "username")]
        public string Username { get; set; }
    }
}
